# Домашнее задание
## к модулю #4 DVC+S3. Python пакеты и CLI. Управление зависимостями (Poetry)


Наш проект DOGMA-bot, для которого я сдавал ТЗ в качестве первого домашнего задания, пока находится на стадии сбора датасетов, и в нём нет пока кода, который можно было бы использовать в данном задании. Поэтому здесь я разместил код из одного учебного проекта, который я когда-то делал.

В отличие от прошлого задания, я не стал запрещать прямой пуш в `main`, учитывая, что в этом ДЗ этого не требуется, и учитывая, что с этим репо работаю только я один.

## Комментарии к пунктам домашнего задания
_(Для удобства проверки)_

### 1. Реализовать CLI для основных модулей пайплайна
_смотреть commit_ [Add CLI versions of data processing and model fitting functions](https://gitlab.com/lekrott/ods-mlops-04homework/-/tree/4eb6809dda2177cff19978617d3510b9bf6f2747)

Ключевые функции, участвующие в пайплайне проекта, имеют как обычную версию, возвращающую значения, так и CLI-версию, которая сохраняет выходные артефакты. При этом CLI-версия вызывает у себя внутри обычную версию функции.

#### CLI реализован в следующих модулях:
- `src/data/cli_clean_data.py`
- `src/data/cli_split_train_test.py`
- `src/features/cli_logreg_features.py`
- `src/models/cli_catboost_fit.py`
- `src/models/cli_logreg_fit.py`

### 2. Настроить DVC для хранения данных в S3 и для запуска пайплайна
#### a. Хранение данных в S3
_смотреть commit_ [Put data and models folders to Yandex Cloud S3 storage via DVC](https://gitlab.com/lekrott/ods-mlops-04homework/-/tree/141f5071b06795c11a9b40700f75ba8980be2c8a)
Используется бакет S3 на Yandex Cloud. В файле `.dvc/config` лежат креды для ключа с доступом только на чтение (чтобы можно было делать `dvc pull` в пайплайнах GitLab CI для выполнения тестов). Креды с доступом на запись находятся в файле `.dvc/config.local`, но этот файл не пушится на GitLab.

В этом коммите я вручную добавил в dvc файлы из папок `data/interim` и `data/processed` при помощи `dvc add`, чтобы сохранять их в S3. В следующем коммите они станут генерироваться в процессе выполнения пайплайна, и добавленными вручную останутся только исходные данные в папке `data/raw`.

Данные, хранящиеся в S3, также используются в CI/CD пайплайне на GitLab. Там запускаются тесты, проверяющие значения метрик у сохранённых моделей из папки `models`. Для этого перед выполнением тестов выполняется установка зависимостей, а также `dvc pull` нужных файлов (_см._ .`gitlab-ci.yml`)

#### б. Пайплайн
_смотреть commit_ [Add DVC pipeline to process data and fit models](https://gitlab.com/lekrott/ods-mlops-04homework/-/tree/fcd08a86140368e9b37a638f036e05bb5f1a830b)

Пайплайн выполняет обработку данных и обучение моделей, по следующей схеме:
```
                +------------------------+
                | data/raw/churn.csv.dvc |
                +------------------------+
                             *
                             *
                             *
                      +------------+
                      | clean_data |
                      +------------+*
                   ***               ***
                ***                     ***
              **                           **
+---------------------+              +-----------------+
| catboost_train_test |              | logreg_features |
+---------------------+              +-----------------+
            *                                 *
            *                                 *
            *                                 *
    +--------------+                +-------------------+
    | catboost_fit |                | logreg_train_test |
    +--------------+                +-------------------+
                                              *
                                              *
                                              *
                                        +------------+
                                        | logreg_fit |
                                        +------------+
```
Промежуточные и итоговые данные, а также обученные модели, находятся под контролем dvc и хранятся в бакете S3.

### 3. Сохранить зависимости проекта в соответствующем вашему менеджеру зависимостей формате
Ещё начиная с прошлого ДЗ в проекте используется менеджер зависимостей `poetry`. Сами зависимости проекта во всех коммитах сохранены в файле `poetry.lock`.