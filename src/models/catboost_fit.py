""" Train Catboost classifier
"""
import catboost

CATBOOST_CATEGORY_COLUMNS = [
    "Sex", "IsSeniorCitizen", "HasPartner",
    "HasChild", "HasPhoneService", "HasMultiplePhoneNumbers",
    "HasInternetService", "HasOnlineSecurityService",
    "HasOnlineBackup", "HasDeviceProtection", "HasTechSupportAccess",
    "HasOnlineTV", "HasMovieSubscription", "HasContractPhone", "IsBillingPaperless",
    "PaymentMethod",
]

SEED = 762


def catboost_fit(features, target):
    """Fit catboost classifier on data given

    Args:
        features (pd.DataFrame): train features
        target (pd.Series): train labels

    Returns:
        estimator: fitted catboost model
    """
    catboost_estimator = catboost.CatBoostClassifier(
        cat_features=CATBOOST_CATEGORY_COLUMNS,
        iterations=1700,
        l2_leaf_reg=12,
        learning_rate=0.003,
        random_state=SEED,
        silent=True,
    )
    catboost_estimator.fit(features, target)

    return catboost_estimator
