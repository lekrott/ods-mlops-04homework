"""Fit logistic regression estimator with best hyperparameters
"""
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression

SEED = 762


# pylint: disable=C0103
def logreg_fit(X, y):
    """Grid searches for best hyperparameters and fits logistic regression model

    Args:
        features (pd.DataFrame or np.ndarray): train features to be fitted on
        target (pd.Series or np.ndarray): train labels verctor

    Returns:
        model: ready model fitted using best parametars found
    """
    estimator = LogisticRegression(max_iter=1000, random_state=SEED)

    logreg_estimator = GridSearchCV(
        estimator,
        param_grid={"C": [100, 10, 1, 0.1, 0.01, 0.001]},
        cv=5,
        scoring="roc_auc",
        n_jobs=-1,
        verbose=1,
    )

    logreg_estimator.fit(X, y)

    return logreg_estimator
