"""
CLI version of logreg_fit function.
See arguments specification for details.
"""
import click
import pandas as pd
from joblib import dump

from src.features import split_X_y
from src.models import logreg_fit


# pylint: disable=C0103
@click.command()
@click.argument('train_features_path', type=click.Path(exists=True))
@click.argument('model_path', type=click.Path())
def cli_logreg_fit(train_features_path: str, model_path: str):
    """
    Loads train features from .csv file,
    splits features into X and y parts,
    fits logistic regression classifier,
    then saves fitted model to a file.

    Args:
        train_features_path (str): path to train features data .csv
        model_path (str): path to fitted model save file
    """
    train_data = pd.read_csv(train_features_path)

    train_X, train_y = split_X_y(train_data)

    model = logreg_fit(train_X, train_y)

    dump(model, model_path)


# pylint: disable=E1120
if __name__ == '__main__':
    cli_logreg_fit()
